import React, { Component } from 'react';
import './App.scss';
import GoodsList from './components/GoodsList/GoodsList';

class App extends Component {

  state = {
    onFavorite: [],
    onBusket: [],
  }

  async componentDidMount() {

    if (!localStorage.getItem("ItemFavorite")) {
      localStorage.setItem("ItemFavorite", JSON.stringify(this.state.onFavorite))
    }
    if (!localStorage.getItem("ItemBasket")) {
      localStorage.setItem("ItemBasket", JSON.stringify(this.state.onBusket))
    }

    if (JSON.parse(localStorage.getItem("FavoriteItems")) === null) {
      this.setState({ onFavorite: [] })
    } else {
      this.setState({ onFavorite: JSON.parse(localStorage.getItem("FavoriteItems")) })
    }

    if (JSON.parse(localStorage.getItem("InBusketItems")) === null) {
      this.setState({ onBusket: [] })
    } else {
      this.setState({ onBusket: JSON.parse(localStorage.getItem("InBusketItems")) })
    }
  }

  render() {

    const { goods } = this.state;
    return (
      <div className="wrapper">
        <h1 className="main-title">Welcome to HEADSETS STORE! Best choise and max-waranty</h1>
        <GoodsList
          goods={goods}
        />
      </div>
    );
  }

}

export default App;
