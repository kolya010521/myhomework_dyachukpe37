import React, { Component } from "react";
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import "./GoodsItem.scss"

class GoodsItem extends Component {
  state = {
    modalOpen: false,
    onFavorite: false,
  }
  componentDidMount() {
    const { item } = this.props;
    const fav = JSON.parse(localStorage.getItem("ItemFavorite"));
    if (fav) {
      if (fav.includes(item.id)) {
        this.setState({ onFavorite: true });
      }
    }
  }
  openModal = () => {
    this.setState({ modalOpen: true })
  }

  sendInCart = () => {
    const { item } = this.props;
    const cart = JSON.parse(localStorage.getItem("ItemBasket"));
    if (cart) {
      if (cart.length === 0) {
        localStorage.setItem("ItemBasket", JSON.stringify([item.id]));
      } else if (!cart.includes(item.id)) {
        cart.push(item.id);
        localStorage.setItem("ItemBasket", JSON.stringify(cart));
      }
    }
    this.setState({ modalOpen: false });
  }

  closeModal = () => {
    this.setState({ modalOpen: false });
  }

  addToFav = () => {
    const { item } = this.props;
    const fav = JSON.parse(localStorage.getItem("ItemFavorite"));
    if (fav) {
      if (fav.length === 0) {
        localStorage.setItem("ItemFavorite", JSON.stringify([item.id]));
      } else if (!fav.includes(item.id)) {
        fav.push(item.id);
        localStorage.setItem("ItemFavorite", JSON.stringify(fav));
      }
    }
    this.setState({ onFavorite: true });
  }

  removeFromFav = () => {
    const { item } = this.props;
    const fav = JSON.parse(localStorage.getItem("ItemFavorite"));
    const newFav = fav.map((i) => {
      if (i !== item.id) {
        return i;
      }
    })
    const filteredNewFav = newFav.filter(element => element != null)
    localStorage.setItem("ItemFavorite", JSON.stringify(filteredNewFav));
    this.setState({ onFavorite: false });
  }

  render() {
    // const {item: {id, price, url, name, color, article},item, functionClick, onFavorite, onBusket, showModal, addToBusket} = this.props;
    const { item: { id, price, url, name, color, article }, item } = this.props;
    const { modalOpen, onFavorite } = this.state;
    return (
      <div className="product">
        <div className="product-favorite">

          {onFavorite ? <span onClick={this.removeFromFav} className="star">★</span> : <span onClick={this.addToFav} className="star">☆</span>}

        </div>
        <img className="product-image" src={url} />
        <p className="product-article">article: {article}</p>
        <h2 className="product-name">{name}, {color}</h2>
        <p className="product-price">{price} ₴</p>
        <Button text="Add to cart" backgroundColor='green' functionClick={this.openModal} />
        {modalOpen && <Modal
          item={item}
          addToBusket={this.sendInCart}
          functionClose={this.closeModal}
          header="Do you wan't to add this product in busket?"
          closeButton={true}
          textContent="Plase this product on busket?"
        />}
      </div>
    )
  }
}
GoodsItem.propTypes = {
  item:  PropTypes.object,
  key: PropTypes.number
}
GoodsItem.defaultProps = {
  item:  {},
  key: 1
}

export default GoodsItem;