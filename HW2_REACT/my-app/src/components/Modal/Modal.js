import React, { Component } from 'react'
import "./Modal.scss"
import Button from '../Button/Button';
import PropTypes from 'prop-types'
import {ReactComponent as CloseIcon} from "../../assets/close.svg"


class Modal extends Component {
  render() {
      const {functionClose, header, closeButton, textContent, addToBusket} = this.props;
      return (
         <div className="modal" onClick={functionClose}>
              <div className="modal-inner" onClick={(e) => e.stopPropagation()}>
                  <div className="modal-header">
                     {header}
                     {closeButton && <CloseIcon className="close-svg" onClick={functionClose}/>}
                  </div>

                  <div className="modal-content">
                      {textContent}
                  </div>

                  <div className="modal-footer">
                  <div>
                        <Button text="OK" backgroundColor={"#1dc207"} functionClick={addToBusket}/>
                        <Button text="Cancel" backgroundColor={"#1dc207"} functionClick={functionClose}/>
                   </div>
                  </div>
              </div>
          </div>
      )
  }
}
Modal.propTypes = {
  functionClose: PropTypes.func,
  header: PropTypes.string,
  closeButton: PropTypes.func,
  textContent: PropTypes.string,
  addToBusket: PropTypes.func
}
Modal.defaultProps = {
  functionClose: ()=>{},
  header: "Something went wrong",
  closeButton: ()=>{},
  textContent: "Something went wrong",
  addToBusket: ()=>{}
}

export default Modal;