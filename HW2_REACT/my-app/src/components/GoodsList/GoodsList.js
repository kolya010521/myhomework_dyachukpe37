import React, { PureComponent } from "react";
import PropTypes from 'prop-types';
import GoodsItem from "../GoodsItem/GoodsItem";
import "./GoodsList.scss"

class GoodsList extends PureComponent {
  state = {
    goods: [],
  }
  async componentDidMount() {
    const goods = await fetch('data.json').then(response => response.json())
    this.setState({ goods });
  }
  render() {
    const {goods} = this.state;
    return (
      <div className="products">
        <div className="products-list">
          {goods && goods.map((item) =>
            <GoodsItem
              item={item}
              key={item.id}
            />)}
        </div>
      </div>
    )
  }
}
export default GoodsList;