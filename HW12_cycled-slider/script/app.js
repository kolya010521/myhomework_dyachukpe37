let images = document.querySelectorAll('.image-to-show');
let imgsLength = images.length - 1;
let i = 0;
let startBtn = document.querySelector('.btn-start');
let stopBtn = document.querySelector('.btn-stop')

function changeImg(array, index) {
    let active = document.querySelector('.active');

    if (active) {
        active.classList.remove('active');
    }

    array[index].classList.add('active');
    if (i < imgsLength) {
        i++;
    } else {
        i = 0;
    }
};
let imgInterval = setInterval(() => changeImg(images, i), 3000);

startBtn.addEventListener("click", () => {
    imgInterval = setInterval(() => changeImg(images, i), 3000)
    startBtn.setAttribute("disabled", "disabled");
    stopBtn.removeAttribute("disabled", "disabled");
});
stopBtn.addEventListener("click", () => {
    clearInterval(imgInterval);
    startBtn.removeAttribute("disabled", "disabled");
    stopBtn.setAttribute("disabled", "disabled");

});