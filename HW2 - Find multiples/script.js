let userNum = +prompt("Введите ваше число!");
let iterations = 0;
/* Проверка числа целочисловое ли оно, если нет повторяем ввод*/
while (userNum !== parseInt(userNum)) {
    userNum = +prompt("Давайте попробуем еще раз! Введите ваше число!");
}
//Так как  0 % 5 = 0; И в тз написано "от 0 до введенного пользователем числа."
if (userNum >= 0) {
    for (let i = 0; i <= userNum; i++) {
        if (i % 5 === 0) {
            console.log(i);
            iterations++;
        }
    } //for
    if (iterations === 0) {
        alert("Sorry, no numbers");
    }
} //if с проверкой
else if (userNum < 0) {
    for (let i = 0; i >= userNum; i--) {
        if (i % 5 === 0 && i !== 0) {
            console.log(i);
            iterations++;
        }
    } //for
    if (iterations === 0) {
        alert("Sorry, no numbers");
    }
} else {
    alert("Sorry, something wrong!");
}
