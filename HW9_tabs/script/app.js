let tabsLi = document.querySelectorAll(".tabs-title");
let tabsLiText = document.querySelectorAll(".tabs-content-item");

tabsLi.forEach(function(elem){
    elem.addEventListener("click", function(){
        let currentLi = elem;
        let tabId = currentLi.getAttribute("data-title");
        let currentTab = document.querySelector(tabId);
        tabsLi.forEach(function(elem){
            elem.classList.remove("active");
        })
        tabsLiText.forEach(function(elem){
            elem.classList.remove("active");
        })
        currentLi.classList.add("active")
        currentTab.classList.add("active")
    })
})