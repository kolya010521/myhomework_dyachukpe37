let tabsLi = document.querySelectorAll(".tabs-title");
let tabsLiText = document.querySelectorAll(".tabs-content-item");
let filterTabsUl = document.querySelector(".amazing_tabs");
let filterTabs = document.querySelectorAll(".amazing-tabs-title");
let allAmazingImages = document.querySelector(".all_images")
let allImagesContainer = document.querySelectorAll(".all_images_item")
let loadBtn = document.querySelector(".load_btn");
let filterTabsItems = document.querySelectorAll(".amazing-tabs-content-item");
let amazingWrapper = document.querySelector("amazing-wrapper");


tabsLi.forEach(function (elem) {
    elem.addEventListener("click", function () {
        let currentLi = elem;
        let tabId = currentLi.getAttribute("data-title");
        let currentTab = document.querySelector(tabId);
        tabsLi.forEach(function (elem) {
            elem.classList.remove("active");
        })
        tabsLiText.forEach(function (elem) {
            elem.classList.remove("active");
        })
        currentLi.classList.add("active")
        currentTab.classList.add("active")
    })
})

filterTabs.forEach(function (elem) {
    elem.addEventListener("click", function () {
        let currentLi = elem;
        let tabId = currentLi.getAttribute("data-title");
        let currentTab = document.querySelector(tabId);
        filterTabs.forEach(function (elem) {
            elem.classList.remove("active");
        })
        filterTabsItems.forEach(function (elem) {
            elem.classList.remove("active");
        })
        currentLi.classList.add("active")
    })
})

function filter() {
    filterTabsUl.addEventListener("click", event => {
        const targetId = event.target.dataset.id;
        switch (targetId) {
            case "all":
                getItems("all")
                break
            case "graphic_des":
                getItems(targetId)
                break
            case "web_des":
                getItems(targetId)
                break
            case "landing":
                getItems(targetId)
                break
            case "wordpress":
                getItems(targetId)
                break
        }
    })
}
filter();

function getItems(className) {
    allImagesContainer.forEach(item => {
        if (item.classList.contains(className)) {
            item.style.display = "block"
        } else {
            item.style.display = "none"
        }
    })
    loadBtn.remove();
}

loadBtn.addEventListener("click", () => {
    allImagesContainer.forEach(elem => elem.style.display = "block")
    loadBtn.remove();
})


// Slider

const smallSlide = document.querySelectorAll(".person_small_block");
const personCard = document.querySelectorAll(".person_item");
const cursorBtn = document.querySelectorAll('.cursor');

smallSlide.forEach(item => {
    item.addEventListener("click", function () {
        let activePerson = item;
        let person = activePerson.getAttribute("data-slide");
        let activePic = document.querySelector(person);

        if (!activePerson.classList.contains("active_person")) {
            smallSlide.forEach((item) => {
                item.classList.remove("active_person");
            });

            personCard.forEach((item) => {
                item.classList.remove("active_person");
            });

            activePerson.classList.add("active_person");
            activePic.classList.add("active_person");
        }
    });
});

document.querySelector(".person_small_block").click();


const nav = document.querySelectorAll('.person_small_block');
const slides = document.querySelectorAll('.person_item');
const rightCursor = document.querySelector('.right-cursor');
const leftCursor = document.querySelector('.left-cursor');

let currentSlide = 0;

nav.forEach((item, i) => {
    item.addEventListener('click', () => {
        currentSlide = i;
        nav[currentSlide].classList.add('active_person');
        slides[currentSlide].classList.add('active_person');

    });

});

rightCursor.addEventListener('click', () => {
    nextSlide(currentSlide);
});

leftCursor.addEventListener('click', () => {
    previousSlide(currentSlide);
});

function nextSlide() {
    goToSlide(currentSlide + 1);
}

function previousSlide() {
    goToSlide(currentSlide - 1);
}

function goToSlide(n) {
    hideSlides();
    currentSlide = (n + slides.length) % slides.length;
    showSlides();
}

function hideSlides() {
    slides[currentSlide].className = 'person_item';
    nav[currentSlide].className = 'person_small_block';
}

function showSlides() {
    slides[currentSlide].className = 'person_item active_person';
    nav[currentSlide].className = 'person_small_block active_person';
}