//      Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
//      В JS когда мы хотим прочитать свойство или метод из object , а оно/он отсутствует, JavaScript идет по каскаду прототипов ищет его, если находит использует,
//      если такого свойства нет не у самого обьектиа не у его прототипов - будет ошибка.


class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get empName() {
        return this.name ;
    }
    set empName(value) {
        this.name = value;
    }
    get empAge() {
        return this.age ;
    }
    set empAge(value) {
        this.age = value;
    }
    get empSalary() {
        return this.salary;
    }
    set empSalary(value) {
        this.salary = value;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this.lang = lang;
    }
    get empSalary() {
        return this.salary * 3;
    }
}

const olegProger = new Programmer( "Oleg", 29, 10000, "C++");
const valeraProger = new Programmer( "Valera", 31, 8000, "C++");
console.log(olegProger);
console.log(valeraProger);