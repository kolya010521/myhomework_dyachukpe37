/*  Опишите своими словами, как Вы понимаете, что такое обработчик событий.
Обработчик событий, ето функция которая сробатывает при заданом нами событии    */

//Создаем переменныe с елементами розметки
let div = document.createElement("div");
let input = document.createElement("input");
let label = document.createElement("label");
let span = document.createElement("span");
let para = document.createElement("p");
let btn = document.createElement("button");
//Задаем стили, добавляем статичные елементы в розметку
span.style.display = "inline-block";
input.id = "input";
label.textContent = "Price: ";
para.style.display = "inline";
para.textContent = "Please enter correct price";
document.body.prepend(div);
div.insertAdjacentElement("afterbegin", label);
div.insertAdjacentElement("beforeend", input);

//Добавляем обработчики событий фокуса и миссфокуса 
input.addEventListener("focus", function(){
    input.style.borderColor = "green";
    input.style.borderWidth = "5px";
    span.remove();
    btn.remove();
    para.remove();
    input.style.color = ""; 
})

input.addEventListener("blur", () => {
    input.style = "";
    if (input.value > 0)  {
    para.remove();
    span.textContent = `Текущая цена: ${input.value}$`;
    btn.textContent = "X";
    btn.style.marginLeft = "5px";
    div.style.marginTop = "5px";
    document.body.prepend(btn);
    document.body.prepend(span);
    input.style.color = "green";
    btn.addEventListener("click", () => {
        span.remove();
        btn.remove();
        input.value = "";
    })
    }
    else if(input.value === ""){
        para.remove();
    }
    else {
        input.style.borderColor = "red";
        input.style.borderWidth = "5px";
        div.after(para);
    }
})
