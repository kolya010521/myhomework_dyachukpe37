const find = document.querySelector('.find');
const btn = document.querySelector('#find__btn');
const ul = document.createElement('ul');
ul.classList.add('find__list');

btn.addEventListener('click', async() => {

    const urlId = 'https://api.ipify.org/?format=json';
    const response = await fetch(urlId);
    const responseIdInfo = await response.json();

    const ip = responseIdInfo.ip;
    const urlAddres = `http://ip-api.com/json/${ip}`;
    const responseAddres = await fetch(urlAddres);
    const responseAddresInfo = await responseAddres.json();

    const { timezone, country, isp, city, zip, query} = responseAddresInfo;

    ul.insertAdjacentHTML('beforeend', `<li> Континент: ${timezone}</li>
    <li> Страна: ${country}</li>
    <li> Город: ${city}</li>
    <li> ZIP-код района: ${zip}</li>
    <li> ИСП: ${isp}</li>
    <li> IP-адресс: ${query}</li>`);

    find.append(ul);
})